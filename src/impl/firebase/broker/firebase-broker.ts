import { addDoc, onSnapshot } from 'firebase/firestore'
import { getCandidatesRef, getDescriptionsRef } from '../utils'
import { CandidateInfo, SimpleRTCSessionDescription } from './types'
import {
    Broker,
    BrokerContext,
    CandidateHandler,
    DescriptionHandler,
} from '../../../interfaces/broker.interfaces'
import EventEmitter2 from 'eventemitter2'

interface FirebaseBrokerOptions {
    debug?: boolean
}

class FirebaseBroker extends EventEmitter2 implements Broker {
    private _sessionId: string
    private _ctx: BrokerContext

    private _debug!: (msg: string) => void

    constructor(sessionId: string, options?: FirebaseBrokerOptions) {
        super()
        this.setDebug(options)
        this._sessionId = sessionId
        this._ctx = {}
    }

    setContext(ctx: BrokerContext) {
        this._ctx = ctx
    }

    async sendIceCandidate(
        candidate: RTCIceCandidate,
        { senderId, recipientId, senderClientId }: BrokerContext
    ) {
        const ref = getCandidatesRef(this._sessionId)
        const candidateData = candidate.toJSON() as CandidateInfo
        this._debug?.(
            'firebase broker: send candidate Data: ' +
                JSON.stringify({
                    candidateData,
                    senderId,
                    recipientId,
                    senderClientId,
                })
        )
        await addDoc(ref, {
            ...candidateData,
            senderClientId,
            senderId,
            recipientId,
        })
    }

    async sendDescription(
        description: RTCSessionDescription,
        { senderId, recipientId, senderClientId }: BrokerContext
    ) {
        const ref = getDescriptionsRef(this._sessionId)
        const descriptionData =
            description.toJSON() as SimpleRTCSessionDescription
        this._debug?.(
            'firebase broker: send description Data: ' +
                JSON.stringify({
                    descriptionData,
                    senderId,
                    recipientId,
                    senderClientId,
                })
        )
        await addDoc(ref, {
            ...descriptionData,
            senderClientId,
            senderId,
            recipientId,
        })
    }

    registerOnCandidateHandler(handler: CandidateHandler) {
        const self = this
        const ref = getCandidatesRef(self._sessionId)
        this._debug?.('firebase broker: register OnCandidateHandler')
        onSnapshot(ref, (querySnapshot) => {
            querySnapshot.docChanges().forEach(async (change) => {
                if (change.type === 'added') {
                    const {
                        senderId,
                        recipientId,
                        senderClientId,
                        ...candidate
                    } = change.doc.data()
                    if (recipientId === self._ctx.senderId) {
                        self._debug?.('execute OnCandidateHandler')
                        await handler(new RTCIceCandidate(candidate), {
                            senderId,
                            recipientId,
                            senderClientId,
                        })
                    }
                }
            })
        })
    }

    registerDescriptionHandler(handler: DescriptionHandler) {
        const self = this
        const ref = getDescriptionsRef(self._sessionId)
        this._debug?.('firebase broker: register OnDescriptionHandler')
        onSnapshot(ref, (querySnapshot) => {
            querySnapshot.docChanges().forEach(async (change) => {
                if (change.type === 'added') {
                    const {
                        senderId,
                        recipientId,
                        senderClientId,
                        ...description
                    } = change.doc.data()
                    if (recipientId === self._ctx.senderId) {
                        self._debug?.(
                            'firebase broker: execute OnDescriptionHandler'
                        )
                        await handler(
                            new RTCSessionDescription(
                                description as RTCSessionDescriptionInit
                            ),
                            { senderId, recipientId, senderClientId }
                        )
                    }
                }
            })
        })
    }

    private setDebug(options?: FirebaseBrokerOptions) {
        if (options?.debug) {
            let self = this
            self._debug = (msg: string) => {
                //console.log('broker', msg)
                self.emit('debug', 'broker', msg)
            }
        }
    }
}

export { FirebaseBroker }
