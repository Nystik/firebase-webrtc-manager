import { collection, getFirestore } from 'firebase/firestore'

const getCandidatesRef = (negotiationId: string) => {
    return collection(
        getFirestore(),
        `RTCNegotiations`,
        negotiationId,
        'RTCCandidates'
    )
}

export { getCandidatesRef }
