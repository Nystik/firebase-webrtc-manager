type SimpleRTCSessionDescription = {
    sdp: string
    type: RTCSdpType
}

type CandidateInfo = {
    candidate: string
    sdpMid: string
    sdpMLineIndex: number
}

export { SimpleRTCSessionDescription, CandidateInfo }
