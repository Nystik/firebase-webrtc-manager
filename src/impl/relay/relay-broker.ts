import EventEmitter2 from 'eventemitter2'
import {
    Broker,
    BrokerContext,
    CandidateHandler,
    DescriptionHandler,
} from '../../interfaces'
import {
    BootstrapCandidate,
    BootstrapDescription,
    BootstrapMessage,
    isDescriptionMessage,
} from '../bootstrap/bootstrap-broker'

type ChannelReadyMessage = {
    type: 'ready'
    clientId: string
}

type DiscoveryMessage = {
    type: 'relay-discover'
    peers: string[]
}

type RelayJoinMessage = {
    type: 'relay-join'
    peerId: string
    relaySender: string
    relayTarget: string
}

type RelaySignalMessage = {
    type: 'signal'
    msg: BootstrapMessage
    relaySender: string
    relayTarget: string
}

type RelayMessage =
    | DiscoveryMessage
    | RelaySignalMessage
    | ChannelReadyMessage
    | RelayJoinMessage

interface RelayBrokerOptions {
    debug?: boolean
}

class RelayBroker extends EventEmitter2 implements Broker {
    private channel: RTCDataChannel
    private localClientId: string
    private remoteClientId: string

    private ctx: BrokerContext = {}

    private _debug!: (msg: string) => void

    private messageHandler: {
        description?: DescriptionHandler
        candidate?: CandidateHandler
    }

    constructor(
        channel: RTCDataChannel,
        localClientId: string,
        remoteClientId: string,
        options?: RelayBrokerOptions
    ) {
        super()

        this.channel = channel
        this.messageHandler = {}
        this.localClientId = localClientId
        this.remoteClientId = remoteClientId

        this.setDebug(options)

        this.setOnMessage()
    }

    async sendDescription(
        description: RTCSessionDescription,
        context: BrokerContext
    ): Promise<void> {
        let descriptionMsg: BootstrapDescription = {
            type: 'description',
            data: description.toJSON(),
            context,
        }

        let msg: RelaySignalMessage = {
            msg: descriptionMsg,
            type: 'signal',
            relaySender: this.localClientId,
            relayTarget: this.remoteClientId,
        }
        this.channel.send(JSON.stringify(msg))
    }

    async sendIceCandidate(
        candidate: RTCIceCandidate,
        context: BrokerContext
    ): Promise<void> {
        let candidateMsg: BootstrapCandidate = {
            type: 'candidate',
            data: candidate.toJSON(),
            context,
        }

        let msg: RelaySignalMessage = {
            msg: candidateMsg,
            type: 'signal',
            relaySender: this.localClientId,
            relayTarget: this.remoteClientId,
        }
        this.channel.send(JSON.stringify(msg))
    }

    registerOnCandidateHandler(handler: CandidateHandler): void {
        this.messageHandler.candidate = handler
    }

    registerDescriptionHandler(handler: DescriptionHandler): void {
        this.messageHandler.description = handler
    }

    setContext(ctx: BrokerContext): void {
        this.ctx = ctx
    }

    private setOnMessage() {
        this.channel.addEventListener('message', this.onRelayMessage.bind(this))
    }

    private onRelayMessage(e: MessageEvent<string>) {
        this._debug?.('relay broker onmessage: ' + JSON.stringify(e))
        const msg: RelayMessage = JSON.parse(e.data)
        switch (msg.type) {
            case 'signal':
                if (msg.msg.context.recipientId === this.ctx.senderId) {
                    this.handleSignal(msg)
                }
                break
            default:
                break
        }
    }

    private handleSignal({ msg }: RelaySignalMessage) {
        if (isDescriptionMessage(msg)) {
            let handler = this.messageHandler.description
            if (handler) {
                handler(msg.data, msg.context)
            }
        } else {
            let handler = this.messageHandler.candidate
            if (handler) {
                handler(msg.data as RTCIceCandidate, msg.context)
            }
        }
    }

    private setDebug(options?: RelayBrokerOptions) {
        if (options?.debug) {
            let self = this
            self._debug = (msg: string) => {
                //console.log('broker', msg)
                self.emit('debug', 'broker', msg)
            }
        }
    }
}

export {
    RelayBroker,
    RelayMessage,
    RelaySignalMessage,
    DiscoveryMessage,
    ChannelReadyMessage,
    RelayJoinMessage,
}
