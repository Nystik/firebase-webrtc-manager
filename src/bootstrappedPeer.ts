import { v4 as uuid } from 'uuid'
import { Broker, BrokerContext } from './interfaces/broker.interfaces'
import { BootstrapBroker } from './impl/bootstrap/bootstrap-broker'
import EventEmitter2, { Listener, ListenerFn, OnOptions } from 'eventemitter2'

interface BootstrappedPeerOptions {
    initiator: boolean
    debug?: boolean
    rtcConfig?: RTCConfiguration
}

const bootstrapChannelName = 'bootstrap-signal'

class BootstrappedPeer extends RTCPeerConnection {
    private _broker!: Broker
    private _polite: boolean = false
    private _makingOffer: boolean = false
    private _ignoreOffer: boolean = false

    private _emitter: EventEmitter2 = new EventEmitter2()

    private _signalChannel?: RTCDataChannel

    private _debug!: (tag: string, msg: string) => void

    localId: string
    localClientId: string
    remoteId?: string
    remoteClientId?: string
    status: string

    constructor(
        broker: Broker,
        clientId: string,
        options?: BootstrappedPeerOptions
    ) {
        super(options?.rtcConfig)
        let p = new RTCPeerConnection()
        this.localId = uuid()
        this.localClientId = clientId

        this._polite = options?.initiator ? true : false

        this.status = 'pending'

        this.setDebug(options)

        this.setBroker(broker)
    }

    listen() {
        if (this.status !== 'signaling') {
            this.status = 'waiting for signal'
        }
        this.setOnNegotiationNeeded()
        this.setIceCandidateHandler()
        this.setOnDataChannel()
    }

    signal(remoteId: string) {
        this.remoteId = remoteId
        this.status = 'signaling'
        this.listen()
        this.createBootstrapChannel()
    }

    setBroker(broker: Broker) {
        broker.setContext({
            senderId: this.localId,
            recipientId: this.remoteId,
            senderClientId: this.localClientId,
        })
        broker.registerDescriptionHandler(this.OnDescriptionMessage())
        broker.registerOnCandidateHandler(this.OnCandidateMessage())
        this._broker = broker
    }

    getBroker(): Broker {
        return this._broker
    }

    // Event emitter
    on(
        event: string | symbol | (string | symbol)[],
        listener: ListenerFn,
        options?: boolean | OnOptions
    ): EventEmitter2 | Listener {
        return this._emitter.on(event, listener, options)
    }

    private setDebug(options?: BootstrappedPeerOptions) {
        if (options?.debug) {
            let self = this
            self._debug = (tag: string, msg: string) => {
                //console.log(tag, msg)
                self._emitter.emit('debug', tag, msg)
            }
        }
    }

    private createBootstrapChannel() {
        this._debug?.('peer', 'createBootstrapChannel')
        let signal_channel = this.createDataChannel(bootstrapChannelName)

        let self = this
        signal_channel.onopen = (e: any) => {
            self._debug?.('peer', 'signal channel onopen: ' + JSON.stringify(e))
            self.setBroker(
                new BootstrapBroker(signal_channel, { debug: !!this._debug })
            )
            self.status = 'connected'
            self._emitter.emit('connected', this)
        }
        signal_channel.onclose = (e: any) => {
            self.status = 'disconnected'
            self._debug?.(
                'peer',
                'signal channel onclose: ' + JSON.stringify(e)
            )
        }

        this._signalChannel = signal_channel
    }

    private setOnDataChannel() {
        const self = this
        self.ondatachannel = (e) => {
            console.log('bootstrappeer ondatachannel')
            self._debug?.('peer', 'data channel connected: ' + e.channel.label)
            if (e.channel.label === bootstrapChannelName) {
                self._signalChannel = e.channel
                this.setBroker(new BootstrapBroker(self._signalChannel))
                this._emitter.emit('connected', this)
            } else {
                self._emitter.emit('datachannel', e)
            }
        }
    }

    private isOfferCollison(description: RTCSessionDescription) {
        return (
            description.type == 'offer' &&
            (this._makingOffer || this.signalingState != 'stable')
        )
    }

    private OnDescriptionMessage() {
        const self = this
        return async (
            description: RTCSessionDescription,
            { senderId, recipientId, senderClientId }: BrokerContext
        ) => {
            self._debug?.(
                'peer',
                'onDescriptionMessage: ' +
                    JSON.stringify({ description, senderId, recipientId })
            )
            self._ignoreOffer =
                !self._polite && self.isOfferCollison(description)

            if (self._ignoreOffer) {
                return
            }

            // if we haven't set the remote id for this connection, do so now.
            if (!self.remoteId) {
                self.remoteId = senderId
            }
            if (!self.remoteClientId) {
                self.remoteClientId = senderClientId
            }

            await self.setRemoteDescription(description)

            if (description.type == 'offer' && self._broker) {
                await self.setLocalDescription()
                self._broker.sendDescription(self.localDescription!, {
                    senderId: self.localId,
                    senderClientId: self.localClientId,
                    recipientId: self.remoteId,
                })
            }
        }
    }

    private OnCandidateMessage() {
        const self = this
        return async (
            candidate: RTCIceCandidateInit,
            { senderId, recipientId }: BrokerContext
        ) => {
            self._debug?.(
                'peer',
                'onDescriptionMessage: ' +
                    JSON.stringify({ candidate, senderId, recipientId })
            )
            try {
                await self.addIceCandidate(candidate)
            } catch (err) {
                if (!self._ignoreOffer) {
                    throw err
                }
            }
        }
    }

    private setOnNegotiationNeeded() {
        const self = this
        self.onnegotiationneeded = async () => {
            self._debug?.('peer', 'onnegotiationneeded')
            try {
                self._makingOffer = true
                await self.setLocalDescription()
                self._broker.sendDescription(self.localDescription!, {
                    senderId: self.localId,
                    senderClientId: self.localClientId,
                    recipientId: self.remoteId,
                })
            } catch (err) {
                console.error(err)
            } finally {
                self._makingOffer = false
            }
        }
    }

    private setIceCandidateHandler() {
        const self = this
        self.onicecandidate = ({ candidate }) => {
            self._debug?.(
                'peer',
                'onicecandidate: ' + JSON.stringify(candidate)
            )
            if (candidate) {
                self._broker.sendIceCandidate(candidate, {
                    senderId: self.localId,
                    senderClientId: self.localClientId,
                    recipientId: self.remoteId,
                })
            }
        }
    }
}

export { BootstrappedPeer, BootstrappedPeerOptions }
