import EventEmitter2 from 'eventemitter2'
import {
    Broker,
    BrokerContext,
    CandidateHandler,
    DescriptionHandler,
} from '../../interfaces'

interface BootstrapBrokerOptions {
    debug?: boolean
}

type BootstrapCandidate = {
    type: 'candidate'
    data: RTCIceCandidateInit
    context: BrokerContext
}

type BootstrapDescription = {
    type: 'description'
    data: RTCSessionDescription
    context: BrokerContext
}

type BootstrapMessage = BootstrapDescription | BootstrapCandidate

const isDescriptionMessage = (
    msg: BootstrapMessage
): msg is BootstrapDescription => {
    return msg.type === 'description'
}

class BootstrapBroker extends EventEmitter2 implements Broker {
    private channel: RTCDataChannel
    private messageHandler: {
        description?: DescriptionHandler
        candidate?: CandidateHandler
    }

    private _debug!: (msg: string) => void

    constructor(channel: RTCDataChannel, options?: BootstrapBrokerOptions) {
        super()
        this.setDebug(options)
        this.channel = channel
        this.messageHandler = {}

        this.setOnMessage()
    }

    async sendDescription(
        description: RTCSessionDescription,
        context: BrokerContext
    ): Promise<void> {
        this._debug?.('bootstrap broker: send description')
        let msg: BootstrapDescription = {
            data: description.toJSON(),
            type: 'description',
            context,
        }
        this.channel.send(JSON.stringify(msg))
    }

    async sendIceCandidate(
        candidate: RTCIceCandidate,
        context: BrokerContext
    ): Promise<void> {
        this._debug?.('bootstrap broker: send ice candidate')
        let msg: BootstrapCandidate = {
            data: candidate.toJSON(),
            type: 'candidate',
            context,
        }
        this.channel.send(JSON.stringify(msg))
    }

    registerOnCandidateHandler(handler: CandidateHandler): void {
        this.messageHandler.candidate = handler
    }

    registerDescriptionHandler(handler: DescriptionHandler): void {
        this.messageHandler.description = handler
    }

    setContext(ctx: BrokerContext): void {}

    private setOnMessage() {
        this.channel.onmessage = (e) => {
            console.log('bootstrap onmessage', e)
            this._debug?.('bootstrap broker onmessage: ' + JSON.stringify(e))
            const msg: BootstrapMessage = JSON.parse(e.data)

            if (isDescriptionMessage(msg)) {
                let handler = this.messageHandler.description
                if (handler) {
                    handler(msg.data, msg.context)
                }
            } else {
                let handler = this.messageHandler.candidate
                if (handler) {
                    handler(msg.data, msg.context)
                }
            }
        }
    }

    private setDebug(options?: BootstrapBrokerOptions) {
        if (options?.debug) {
            let self = this
            self._debug = (msg: string) => {
                //console.log('broker', msg)
                self.emit('debug', 'broker', msg)
            }
        }
    }
}

export {
    BootstrapBroker,
    BootstrapMessage,
    isDescriptionMessage,
    BootstrapDescription,
    BootstrapCandidate,
}
