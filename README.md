# Firebase WebRTC Mesh

> **NOTE: This is a hobby project with no expectation of support or maintenance.**

## Description

A WebRTC connection manager that uses firestore to negotiate the initial peer connection. Follows the WebRTC [perfect negotiation pattern](https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API/Perfect_negotiation).

After the signaling via firestore is done, the manager performs any further negotiations over an established Data Channel. If more peers join, they do their initial singaling over firestore, and then the "host" peer relays any further signaling and negotiations between the other peers.

## Usage

Remember to initialize your firebase app before using the signaler.

```typescript
import { initializeApp } from 'firebase/app'

const firebaseConfig = {
    apiKey: '<API KEY>',
    ...
}

initializeApp(firebaseConfig)
```

```typescript
//TODO: add working example
```

At this point all offers, answers, and ice candidates are handled by the connection manager and we can add streams and data channels to the peer connection as we wish.

```typescript
...
stream.getTracks().forEach((track) => peer.addTrack(track, stream))
...
```

## Known Issues

-   if you get `Error: No Firebase App '[DEFAULT]' has been created - call Firebase App.initializeApp()` you need to tell your bundler to remove duplicate imports. For rollup add `dedupe: ['firebase']` to the node-resolve plugin options.

## License

This project is licensed under the MIT license, see [LICENSE](https://gitlab.com/Nystik/firebase-webrtc-manager/-/blob/main/LICENSE) for details.
