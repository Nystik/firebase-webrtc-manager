import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import { terser } from 'rollup-plugin-terser'
import typescript from 'rollup-plugin-typescript2'

const production = !process.env.ROLLUP_WATCH

export default [
    {
        input: 'src/index.ts',
        output: {
            dir: 'lib',
            format: 'es',
            exports: 'named',
            sourcemap: !production,
        },
        external: [/^firebase\/.*/],
        plugins: [
            resolve(),
            commonjs(),
            typescript({ clean: true }),

            production && terser(),
        ],
    },
]
