import { collection, getFirestore } from 'firebase/firestore'

const getDescriptionsRef = (negotiationId: string) => {
    return collection(
        getFirestore(),
        `RTCNegotiations`,
        negotiationId,
        'RTCDescriptions'
    )
}

export { getDescriptionsRef }
