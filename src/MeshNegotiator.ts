import EventEmitter2, { Listener } from 'eventemitter2'
import { v4 as uuid } from 'uuid'
import { BootstrappedPeer } from './bootstrappedPeer'
import { Broker, JoinRequest, Session } from './interfaces'

import {
    ChannelReadyMessage,
    DiscoveryMessage,
    RelayBroker,
    RelayJoinMessage,
    RelayMessage,
    RelaySignalMessage,
} from './impl/relay/relay-broker'

type BrokerFactory = (sessionID: string) => Broker

interface PeerChannelPair {
    peer: BootstrappedPeer
    relayChannel?: RTCDataChannel
    host: boolean
}

interface MeshNegotiatorOptions {
    debug?: boolean
    isHost?: boolean
    rtcConfig?: RTCConfiguration
}

interface Negotiator {
    setBrokerFactory(factory: BrokerFactory): void
}

const relayChannelName = 'relay-channel'

class MeshNegotiator<T extends JoinRequest>
    extends EventEmitter2
    implements Negotiator
{
    private rtcConfig?: RTCConfiguration
    private _connectedPeers: { [key: string]: PeerChannelPair } = {}
    private _brokerFactory?: BrokerFactory
    private _session!: Session<T>

    private _isHost: boolean
    private _sessionId!: string

    private _debug!: (msg: string) => void

    clientId: string

    constructor(session: Session<T>, options?: MeshNegotiatorOptions) {
        super()

        this.rtcConfig = options?.rtcConfig
        this._isHost = options?.isHost ?? false
        this.clientId = uuid()

        this.setDebug(options)
        this.setSession(session)
    }

    setBrokerFactory(factory: BrokerFactory): void {
        this._brokerFactory = factory
    }

    host() {
        this._isHost = true
        this._session.registerOnJoinRequestHandler(
            this.onJoinRequest.bind(this)
        )

        this._debug?.(`set handler and host`)
    }

    join() {
        let peer = this.newPeer(this._brokerFactory!, {
            initiator: false,
            host: true,
        })

        peer.listen()

        this._debug?.(
            `send JoinRequest: ${JSON.stringify({ peerId: peer.localId })}`
        )

        this._session.sendJoinRequest({ peerId: peer.localId } as T)
    }

    private setDebug(options?: MeshNegotiatorOptions) {
        if (options?.debug) {
            let self = this
            self._debug = (msg: string) => {
                //console.log('mesh', msg)
                self.emit('debug', 'mesh', msg)
            }
        }
    }

    private setSession(session: Session<T>) {
        this._session = session
        this._sessionId = session.sessionId
    }

    private newPeer(
        factory: BrokerFactory,
        { initiator, host }: { initiator: boolean; host?: boolean }
    ): BootstrappedPeer {
        let peer = new BootstrappedPeer(
            factory(this._sessionId!),
            this.clientId,
            { initiator, debug: !!this._debug, rtcConfig: this.rtcConfig }
        )

        this.emit('new-peer', peer)

        peer.on('connected', this.getOnPeerConnected(host || false).bind(this))

        return peer
    }

    private setupRelayHost(peer: BootstrappedPeer): RTCDataChannel {
        this._debug?.(`setup relay channel to peer (${peer.remoteClientId})`)
        const relayChannel = peer.createDataChannel(relayChannelName)

        relayChannel.onopen = (e) => {
            this._debug?.(`relay to client opened`)
        }

        relayChannel.onmessage = this.hostRelayHandler.bind(this)

        return relayChannel
    }

    private setupDiscoverClient(peer: BootstrappedPeer, host: boolean) {
        this._debug?.(`wait for relay channel to host`)
        let connectionObj: PeerChannelPair = {
            peer,
            host: true,
            relayChannel: undefined,
        }

        console.log('negotiator: register datachannel handler')
        peer.on(
            'datachannel',
            (e: RTCDataChannelEvent) => {
                console.log(
                    'negotiator ondatachannel',
                    e,
                    'relay to host?',
                    host
                )
                if (e.channel.label === relayChannelName && host) {
                    connectionObj.relayChannel = e.channel
                    connectionObj.relayChannel.onmessage =
                        this.clientDiscoverHandler.bind(this)
                    this._connectedPeers[peer.remoteClientId!] = connectionObj
                    this._debug?.(`relay to host established`)
                    this.sendReady(connectionObj.relayChannel)
                } else {
                    this.emit('channel', peer, e)
                }
            },
            true
        ) as Listener
    }

    private tryNewConnection(remoteId: string) {
        let peer = this.newPeer(this._brokerFactory!, {
            initiator: true,
        })

        this._debug?.(`signal (L)${peer.localId} <---> ${remoteId}(R)`)

        peer.signal(remoteId)
    }

    private getOnPeerConnected(host: boolean) {
        return (peer: BootstrappedPeer) => this.peerConnected(peer, host)
    }

    private peerConnected(peer: BootstrappedPeer, host: boolean) {
        this._debug?.(
            `new peer connected(${peer.remoteClientId}): (L)${peer.localId} <---> ${peer.remoteId}(R)`
        )

        //this client (which is not host) has connected to a peer (which is the host)
        let connectionObj: PeerChannelPair = {
            peer,
            host,
            relayChannel: undefined,
        }

        this._debug?.(`peer info: host: ${host}`)

        this.setupDiscoverClient(peer, host)
        if (this._isHost) {
            // since we are the host, setup the relay router
            connectionObj.relayChannel = this.setupRelayHost(peer)
            this._connectedPeers[peer.remoteClientId!] = connectionObj
        }
        this.emit('connected', peer)
    }

    private sendJoinRequest(targetClientId: string, peerId: string) {
        let msg: RelayJoinMessage = {
            type: 'relay-join',
            peerId,
            relaySender: this.clientId,
            relayTarget: targetClientId,
        }
        this._debug?.(`send JoinRequest via relay: ${JSON.stringify(msg)})}`)

        const channel = this.getHostRelay()!

        console.log('send join request', channel, msg)

        channel.send(JSON.stringify(msg))
    }

    private sendReady(relayChannel: RTCDataChannel) {
        let msg: ChannelReadyMessage = {
            type: 'ready',
            clientId: this.clientId,
        }

        this._debug?.(`sending relay ready message ${JSON.stringify(msg)}`)
        relayChannel.send(JSON.stringify(msg))
    }

    private sendDiscoveryData(clientId: string) {
        const pair = this._connectedPeers[clientId]
        if (pair?.relayChannel) {
            let msg: DiscoveryMessage = {
                type: 'relay-discover',
                peers: Object.values(this._connectedPeers)
                    .filter((p) => p.peer.remoteClientId !== clientId)
                    .map((p) => p.peer.remoteClientId!),
            }
            this._debug?.(
                `sending peer discovery message ${JSON.stringify(msg)}`
            )
            pair.relayChannel.send(JSON.stringify(msg))
        }
    }

    private hostRelayHandler(e: MessageEvent<string>) {
        let msg: RelayMessage = JSON.parse(e.data)
        console.log('host relay', msg, e)
        switch (msg.type) {
            case 'ready':
                let { clientId } = msg
                this.sendDiscoveryData(clientId)
                break
            case 'relay-join':
            case 'signal':
                this.routeSignal(msg)
                break
            default:
                break
        }
    }

    private routeSignal(msg: RelaySignalMessage | RelayJoinMessage) {
        this._debug?.(`routing message: ${JSON.stringify(msg)}`)
        const channel = this.getRelay(msg.relayTarget)

        if (channel) {
            channel.send(JSON.stringify(msg))
        }
    }

    private clientDiscoverHandler(e: MessageEvent<string>) {
        // this handler only ever handles the 'discovery' type message.
        // for the handling of signals on clients see the relay broker
        let msg: RelayMessage = JSON.parse(e.data)
        switch (msg.type) {
            case 'relay-join':
                this.handleRelayJoinRequest(msg)
                break
            case 'relay-discover':
                this._debug?.(
                    `received peer discovery message ${JSON.stringify(msg)}`
                )
                this.addDiscoveredPeers(msg.peers)
                break
            default:
                break
        }
    }

    private addDiscoveredPeers(peers: string[]) {
        console.log('discovered peers', peers)
        for (const target of peers) {
            // create new bootstrapped peer with relay broker, passing the target client id, and our local client id
            if (!(target in this._connectedPeers)) {
                console.log('create new peer', target)
                let peer = this.newPeer(
                    this.relayBrokerFactoryFactory(target),
                    {
                        initiator: false,
                    }
                )

                peer.listen()

                this.sendJoinRequest(target, peer.localId)
            }
        }
    }

    private handleRelayJoinRequest(msg: RelayJoinMessage) {
        this._debug?.('relay join message received: ' + JSON.stringify(msg))

        let peer = this.newPeer(
            this.relayBrokerFactoryFactory(msg.relaySender),
            {
                initiator: true,
            }
        )

        this._debug?.(
            `signal (relay) (L)${peer.localId} <---> ${msg.peerId}(R)`
        )

        peer.signal(msg.peerId)
    }

    private onJoinRequest(msg: T) {
        this._debug?.('onJoinRequest: ' + JSON.stringify(msg))

        this.tryNewConnection(msg.peerId)
    }

    private relayBrokerFactoryFactory(targetClientId: string): BrokerFactory {
        const hostRelayChannel = this.getHostRelay()!

        return (): Broker =>
            new RelayBroker(hostRelayChannel, this.clientId, targetClientId)
    }

    private getRelay(clientId: string): RTCDataChannel | undefined {
        return this._connectedPeers[clientId]?.relayChannel
    }

    private getHostRelay(): RTCDataChannel | undefined {
        return Object.values(this._connectedPeers).find((p) => p.host)
            ?.relayChannel
    }
}

export { MeshNegotiator, MeshNegotiatorOptions, BrokerFactory }
