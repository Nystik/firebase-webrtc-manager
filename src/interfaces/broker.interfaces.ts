interface BrokerContext {
    senderId?: string
    recipientId?: string
    senderClientId?: string
}

interface Broker {
    sendDescription(
        description: RTCSessionDescription,
        context: BrokerContext
    ): Promise<void>
    sendIceCandidate(
        candidate: RTCIceCandidate,
        context: BrokerContext
    ): Promise<void>

    registerOnCandidateHandler(handler: CandidateHandler): void
    registerDescriptionHandler(handler: DescriptionHandler): void

    setContext(ctx: BrokerContext): void
}

type CandidateHandler = (
    candidate: RTCIceCandidateInit,
    context: BrokerContext
) => Promise<void> | void

type DescriptionHandler = (
    description: RTCSessionDescription,
    context: BrokerContext
) => Promise<void> | void

export { Broker, BrokerContext, CandidateHandler, DescriptionHandler }
