interface JoinRequest {
    peerId: string
}

interface Session<T extends JoinRequest> {
    sessionId: string
    registerOnJoinRequestHandler(handler: JoinRequestHandler<T>): void
    sendJoinRequest(msg: T): Promise<void>
}

type JoinRequestHandler<T> = (msg: T) => Promise<void> | void

export { Session, JoinRequestHandler, JoinRequest }
