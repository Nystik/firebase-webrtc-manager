import { Unsubscribe, addDoc, onSnapshot } from 'firebase/firestore'
import { createNegotiation, getJoinRequestsRef } from '../utils'
import {
    Session,
    JoinRequestHandler,
    JoinRequest,
} from '../../../interfaces/signaler.interfaces'
import EventEmitter2 from 'eventemitter2'

interface FireBaseJoinRequest extends JoinRequest {}

interface FirebaseSessionOptions {
    debug?: boolean
}

class FireBaseSession
    extends EventEmitter2
    implements Session<FireBaseJoinRequest>
{
    private _debug!: (msg: string) => void

    sessionId: string = ''

    onJoinRequestHandler: {
        handler: JoinRequestHandler<FireBaseJoinRequest>
        unsubscribe: Unsubscribe | undefined
    } = {
        handler: () => {},
        unsubscribe: undefined,
    }

    /***
     * Do not use this constructor,
     * use either create(onJoinRequestHandler: JoinRequestHandler),
     * or listen(sessionId: string)
     */
    constructor(sessionId: string, options?: FirebaseSessionOptions) {
        super()
        this.setDebug(options)
        this.sessionId = sessionId
    }

    static async create(options?: FirebaseSessionOptions) {
        let sessionId = await createNegotiation()
        return new FireBaseSession(sessionId)
    }

    static connect(sessionId: string, options?: FirebaseSessionOptions) {
        const signaler = new FireBaseSession(sessionId)
        return signaler
    }

    registerOnJoinRequestHandler(
        handler: JoinRequestHandler<FireBaseJoinRequest>
    ): void {
        this._debug?.('firebase session: register onJoinRequestHandler')
        const self = this
        // unsubscribe to previous handler
        if (self.onJoinRequestHandler.unsubscribe) {
            self.onJoinRequestHandler.unsubscribe()
        }

        const ref = getJoinRequestsRef(self.sessionId)

        const unsubscribe = onSnapshot(ref, (querySnapshot) => {
            querySnapshot.docChanges().forEach(async (change) => {
                if (change.type === 'added') {
                    const joinRequest = change.doc.data() as FireBaseJoinRequest
                    self._debug?.(
                        'firebase session: executing onJoinRequestHandler'
                    )
                    await self.onJoinRequestHandler.handler(joinRequest)
                }
            })
        })

        self.onJoinRequestHandler = { handler, unsubscribe }
    }

    async sendJoinRequest(joinRequest: FireBaseJoinRequest) {
        this._debug?.(
            'firebase session: send Join request: ' +
                JSON.stringify(joinRequest)
        )
        const ref = getJoinRequestsRef(this.sessionId)

        await addDoc(ref, {
            ...joinRequest,
        })
    }

    private setDebug(options?: FirebaseSessionOptions) {
        if (options?.debug) {
            let self = this
            self._debug = (msg: string) => {
                //console.log('relay-discover', msg)
                self.emit('debug', 'relay-discover', msg)
            }
        }
    }
}

export { FireBaseSession as FireBaseSignaler }
export type { FireBaseJoinRequest }
