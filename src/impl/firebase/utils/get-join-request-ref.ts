import { collection, getFirestore } from 'firebase/firestore'

const getJoinRequestsRef = (negotiationId: string) => {
    return collection(
        getFirestore(),
        `RTCNegotiations`,
        negotiationId,
        'RTCJoinRequests'
    )
}

export { getJoinRequestsRef }
